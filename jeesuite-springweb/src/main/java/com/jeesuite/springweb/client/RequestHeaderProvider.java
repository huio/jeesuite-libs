package com.jeesuite.springweb.client;

import java.util.Map;

public interface RequestHeaderProvider {

	Map<String, String> headers();
}
